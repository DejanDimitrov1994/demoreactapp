import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import CustomerListComponent from './components/customerListComponent';
import AddOrEditCustomerComponent from './components/addOrEditCustomerComponent';

const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={CustomerListComponent} />
            <Route exact path="/customer" component={AddOrEditCustomerComponent} />
            <Route path="/customer/:id" component={AddOrEditCustomerComponent} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default connect()(App);
