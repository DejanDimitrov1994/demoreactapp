import axios from 'axios';

const API_KEY = 'sk_test_0RXp6CBJb8GwLctDfiRiPkOg';
const baseURL = 'https://api.stripe.com/v1/customers';
const config = {
    headers: {
        'Authorization' : 'Bearer ' + API_KEY,
        'Content-Type' : 'application/x-www-form-urlencoded'
    }
};

export const getCustomersResponse = customers => ({
    type: 'GET_CUSTOMERS_RESPONSE',
    customers
});

export function getCustomersAction() {
    return (dispatch) => {

        dispatch(ApiCallBeginAction());

        return axios.get(baseURL, config).then((customers) => {
            dispatch(getCustomersResponse(customers.data.data));
        }).catch((err) => {
            throw err;
        })
    };
}

export const ApiCallBeginAction = () => ({
    type: 'API_CALL_BEGIN'
});

export const ApiCallErrorAction = () => ({
    type: 'API_CALL_ERROR'
});

export const getCustomerResponse = customer => ({
    type: 'GET_CUSTOMER_RESPONSE',
    customer
});

export function deleteCustomerAction(customerId) {
    return (dispatch) => {
        return axios.delete(baseURL + '/' + customerId, config).then((result) => {
           dispatch(getCustomersAction());
        }).catch((error) => {
            throw error;
        });
    }
}

function createFormBody(params, prefix) {
    let items = [];

    for(let field in params) {
        if (field === 'id') {
            continue;
        }

        let key  = prefix ? prefix + "[" + field + "]" : field;
        const type = typeof params[field];

        switch(type) {

            case "object":

                if(params[field].constructor === Array) {
                    params[field].forEach((val) => {
                        items.push(key + "[]=" + encodeURIComponent(val));
                    });
                } else {
                    items = items.concat(createFormBody(params[field], key));
                }
                break;
            case "function":
                break;
            default:
                items.push(key + "=" + encodeURIComponent(params[field]));
                break;
        }
    }

    return items.join("&");
}

export function saveCustomerAction(customer) {
    return (dispatch) => {
        const formBody = createFormBody(customer);
        console.log(formBody);

        if (customer.id) {

            return axios.post(baseURL + '/' + customer.id, formBody, config).then((result) => {

            }).catch((error) => {
                throw error;
            });
        } else {

            return axios.post(baseURL, formBody, config).then((result) => {

            }).catch((error) => {
                throw error;
            })
        }
    }
}

export function getCustomerAction(customerId) {
    return (dispatch) => {

        dispatch(ApiCallBeginAction());

        return axios.get(baseURL + '/' + customerId, config).then((customer) => {
            dispatch(getCustomerResponse(customer.data));
        }).catch((err) => {
            throw err;
        })
    };
}