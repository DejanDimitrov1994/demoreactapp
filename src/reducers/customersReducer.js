export default (state = {}, action) => {
    switch (action.type) {
        case 'API_CALL_BEGIN':
            return {
                loading: true,
                customers: {}
            };
        case 'GET_CUSTOMERS_RESPONSE':
            return {
                loading: false,
                customers: action.customers
            };
        default:
            return {
                ...state
            }
    }
}