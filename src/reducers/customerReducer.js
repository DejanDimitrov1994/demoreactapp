export default (state = {}, action) => {
    switch (action.type) {
        case 'API_CALL_BEGIN':
            return {
                loading: true,
                customer: {}
            };
        case 'GET_CUSTOMER_RESPONSE':
            return {
                loading: false,
                customer: action.customer
            };
        default:
            return {
                ...state
            }
    }
}