import { combineReducers } from 'redux';
import customersReducer from './customersReducer';
import customerReducer from './customerReducer';
import {reducer as formReducer} from 'redux-form';

export default combineReducers({
    customersReducer,
    customerReducer,
    form: formReducer
});