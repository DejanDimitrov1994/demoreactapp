import React from 'react';
import { Field, reduxForm } from 'redux-form';
import FieldInput from './fieldInput';


export const CustomerForm = ({ handleSubmit, pristine, reset, submitting, heading, handleSave, handleCancel }) => {
    return (
        <form onSubmit={handleSubmit(handleSave)}>
            <h1>{heading}</h1>

            <Field
                type="text"
                name="metadata.name"
                label="First Name"
                placeholder="Enter First Name"
                component={FieldInput}
            />

            <Field
                type="text"
                name="metadata.lastName"
                label="Last Name"
                placeholder="Enter Last Name"
                component={FieldInput}
            />

            <Field
                type="text"
                name="email"
                label="E-mail"
                placeholder="Enter e-mail"
                component={FieldInput}
            />

            <Field
                type="text"
                name="description"
                label="Description"
                placeholder="Enter description"
                component={FieldInput}
            />

            <Field
                type="number"
                name="account_balance"
                label="Account Balance"
                placeholder="Enter Account Balance"
                component={FieldInput}
            />

            <div>
                <button type="submit" disabled={submitting} className="btn btn-primary"><i className="fa fa-paper-plane-o" aria-hidden="true" /> Submit</button>

                {heading === 'Add' && <button type="button" disabled={pristine || submitting} onClick={reset} className="btn btn-default btn-space">Clear Values</button>}

                <button type="button" className="btn btn-default btn-space" onClick={handleCancel}>Cancel</button>
            </div>
        </form>
    );
};





const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Required';
    }

    return errors;
};



export default reduxForm({
    form: 'CustomerForm',
    fields: ['name', 'lastName', 'description', 'email', 'account_balance'],
    validate
})(CustomerForm);