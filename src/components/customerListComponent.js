import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as customerAction from '../actions/customerAction';
import CustomerList from './customerList';
import toastr from 'toastr';

class CustomerListComponent extends Component {

    constructor() {
        super();

        this.state = {selectedCustomerId: undefined};

        this.handleAddCustomer = this.handleAddCustomer.bind(this);
        this.handleEditCustomer = this.handleEditCustomer.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleRowSelect = this.handleRowSelect.bind(this);
    }


    componentDidMount() {
        this.props.action.getCustomersAction()
            .catch(error => {
                toastr.error(error);
            });
    }



    handleAddCustomer() {
        this.props.history.push('/customer');
    }



    handleEditCustomer() {
        const { selectedCustomerId } = this.state;

        if (selectedCustomerId) {
            this.setState({selectedCustomerId: undefined});
            this.props.history.push(`/customer/${selectedCustomerId}`);
        }
    }



    handleDelete() {
        const { selectedCustomerId } = this.state;

        if (selectedCustomerId) {
            this.setState({selectedCustomerId: undefined});
            this.props.action.deleteCustomerAction(selectedCustomerId)
                .catch(error => {
                    toastr.error(error);
                });
        }
    }



    handleRowSelect(row, isSelected) {
        if (isSelected) {
            this.setState({selectedCustomerId: row.id});
        }
    }

    render() {
        const { loading, customers } = this.props;

        if (!customers || loading) {
            return (
                <div>Loading...</div>
            );
        }

        return (
            <div className="container-fluid">
                <div className="row mt-3">
                    <div className="col">
                        <h1>Customers</h1>
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col">
                        <div className="btn-group" role="group">
                            <button
                                type="button"
                                className="btn btn-primary"
                                onClick={this.handleAddCustomer}
                            >
                                <i className="fa fa-plus" aria-hidden="true"/> New
                            </button>

                            <button
                                type="button"
                                className="btn btn-warning ml-2"
                                onClick={this.handleEditCustomer}
                            >
                                <i className="fa fa-pencil" aria-hidden="true"/> Edit
                            </button>

                            <button
                                type="button"
                                className="btn btn-danger ml-2"
                                onClick={this.handleDelete}
                            >
                                <i className="fa fa-trash-o" aria-hidden="true" onClick={this.handleDelete}/> Delete
                            </button>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <CustomerList customers={customers} handleRowSelect={this.handleRowSelect}/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        customers: state.customersReducer.customers,
        loading: state.customersReducer.loading
    }
};

const mapDispatchToProps = (dispatch) => ({
    action: bindActionCreators(customerAction, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerListComponent);