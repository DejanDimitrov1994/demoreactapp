import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';


const nameFormatter = (cell, row) => {
    return cell.name ? cell.name : '';
};

const lastNameFormatter = (cell, row) => {
    return cell.lastName ? cell.lastName : '';
};

const balanceFormatter = (cell, row) => {
    return '<i class="glyphicon glyphicon-' + row.currency + '"></i> ' + cell;
};


class CustomerList extends Component {

    constructor(props) {
        super(props);

        this.options = {
            sortIndicator: false,
            noDataText: 'No data'
        };

        this.selectRowProp = {
            mode: 'radio',
            bgColor: '#c1f291',
            onSelect: props.handleRowSelect,
            clickToSelect: true,
            hideSelectColumn: true
        };
    }



    render() {


        return (
            <BootstrapTable data={this.props.customers}  selectRow={this.selectRowProp}  options={this.options} bordered={false} striped hover condensed>
                <TableHeaderColumn dataField="id" isKey hidden>Id</TableHeaderColumn>

                <TableHeaderColumn
                    dataField="metadata"
                    dataFormat={nameFormatter}
                    columnTitle
                >
                    First Name
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="metadata"
                    dataFormat={lastNameFormatter}
                    columnTitle
                >
                    Last Name
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="email"
                    columnTitle
                >
                    Email
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="description"
                    columnTitle
                >
                    Description
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="account_balance"
                    dataFormat={balanceFormatter}
                    columnTitle
                >
                    Balance
                </TableHeaderColumn>

            </BootstrapTable>
        );
    }

}






export default CustomerList;