import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as customerAction from '../actions/customerAction';
import toastr from 'toastr';
import CustomerForm from './customerForm';


export class AddOrEditCustomerComponent extends Component {


    constructor() {
        super();
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }



    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.action.getCustomerAction(this.props.match.params.id)
                .catch(error => {
                    toastr.error(error);
                });
        }
    }



    handleSave(values) {
        const customer = {
            id: values.id,
            metadata: values.metadata,
            description: values.description,
            account_balance: values.account_balance,
            email: values.email
        };

        this.props.action.saveCustomerAction(customer)
            .then(() => {
                toastr.success('Customer saved');
                this.props.history.push('/');
            }).catch(error => {
            toastr.error(error);
        });
    }



    handleCancel(event) {
        event.preventDefault();
        this.props.history.replace('/');
    }



    render() {
        const { initialValues } = this.props;
        const heading = initialValues && initialValues.id ? 'Edit' : 'Add';

        return (
            <div className="container">
                <CustomerForm
                    heading={heading}
                    handleSave={this.handleSave}
                    handleCancel={this.handleCancel}
                    initialValues={this.props.initialValues}
                />
            </div>
        );
    }
}



const mapStateToProps = (state, ownProps) => {
    const customerId = ownProps.match.params.id;

    if (customerId && state.customerReducer.customer && customerId === state.customerReducer.customer.id) {
        return {
            initialValues: state.customerReducer.customer
        };
    } else {
        return {};
    }
};



const mapDispatchToProps = (dispatch) => ({
    action: bindActionCreators(customerAction, dispatch)
});



export default connect(mapStateToProps, mapDispatchToProps)(AddOrEditCustomerComponent);